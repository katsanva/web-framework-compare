const feathers = require('@feathersjs/feathers');
const express = require('@feathersjs/express');

const port = process.env.PORT || 3000;

const app = express(feathers());

app.use(express.json());
app.use(express.urlencoded({ extended: true }));
app.configure(express.rest());

app.use('/hello', {
  find(params) {
    return Promise.resolve('Hello world!');
  },
});

app.use(express.errorHandler());

app.listen(port, () => console.log(`feathers app listening on port ${port}!`));
