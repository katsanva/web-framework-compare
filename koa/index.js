const port = process.env.PORT || 3000;

const Koa = require('koa');
const {get} = require('koa-route');
const app = new Koa();

app.use(get('/hello', async ctx => {
  ctx.body = 'Hello World!';
}));

app.listen(port);

console.log(`koa app listening on port ${port}!`);
