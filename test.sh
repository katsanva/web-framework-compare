#!/usr/bin/env bash

PORT=3000
ROOT=`pwd`
NODE_ENV=production

LOGFILE=$ROOT/result.out

OUTDIR=/tmp/out

rm -rf $OUTDIR
mkdir -p $OUTDIR
STARTPLOT=1

getTiming() {
    APPNAME=`echo $1 | sed 's/\///'`
    echo -e ">>> $APPNAME\n"
    cd $ROOT/$1;
    node index.js &
    PID=$!
    PLOTFILE="$OUTDIR/ab-$APPNAME.dat"
    touch $PLOTFILE
    sleep 10
    echo "ab -m GET -n 1000 -c 100 -Sq -g $PLOTFILE http://localhost:${PORT}/hello"
    ab -m GET -n 10000 -c 100 -Sq -g $PLOTFILE http://127.0.0.1:${PORT}/hello
    echo ">>> Killing pid: $PID"
    kill -9 $PID
    if [ "$STARTPLOT" -eq "1" ]; then
       STARTPLOT=0
       echo -e "plot '$PLOTFILE' using 9 smooth sbezier with lines title '$APPNAME' \\" >> /tmp/plotme
    else
        echo -e ", '$PLOTFILE' using 9 smooth sbezier with lines title '$APPNAME' \\" >> /tmp/plotme
    fi
    sleep 5
}

## Gnuplot settings
echo "set terminal png size 12800,7200
set output '$OUTDIR/benchmark_$(date).png'
set title 'Benchmark: $(date)'
set size 1,1
set grid y
set xlabel 'request'
set ylabel 'response time (ms)' " > /tmp/plotme

for directory in */ ; do
	getTiming $directory
done;

#	getTiming ./express


gnuplot /tmp/plotme


