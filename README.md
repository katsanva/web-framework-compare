# Some comparation of node.js frameworks

## Middleware-like

- express.js
- koa.js
- restify.js
- fastify.js

## All-in-one

- hapi.js
- sails.js
- feathers.js
- loopback

All the projects are bootstrapped using framework-provided tools or taken from official docs
