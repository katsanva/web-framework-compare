const express = require('express');

const port = process.env.PORT || 3000;

const app = express();

app.get('/hello', (req, res) => res.send('Hello World!'));

app.listen(port, () => console.log(`express app listening on port ${port}!`));
