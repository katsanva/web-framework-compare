const fastify = require('fastify')();
const port = process.env.PORT || 3000;

fastify.get('/hello', async (request, reply) => {
  return 'Hello, world!';
});

const start = async () => {
  try {
    await fastify.listen(port);
    console.log(`fastify app listening on port ${fastify.server.address().port}!`)
  } catch (err) {
    fastify.log.error(err);
    process.exit(1)
  }
};

start();
