const restify = require('restify');
const port = process.env.PORT || 3000;

function respond(req, res, next) {
  res.send('Hello, world!');
  next();
}

const server = restify.createServer();

server.get('/hello', respond);

server.listen(port, function() {
  console.log(`restify app listening on port ${port}!`)
});
