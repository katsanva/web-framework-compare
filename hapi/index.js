const port = process.env.PORT || 3000;
const Hapi = require('hapi');

const server = Hapi.server({
  host: 'localhost',
  port
});

server.route({
  method: 'GET',
  path: '/hello',
  handler: function (request, h) {
    return 'Hello world!';
  },
});

async function start() {
  try {
    await server.start();
  } catch (err) {
    console.log(err);
    process.exit(1);
  }

  console.log(`HAPI app listening on port ${port}!`)
}

start();
